package com.gladepay.android.exceptions;

public class InvalidAmountException extends GladepayException{

    private int amount;

    public InvalidAmountException(int amount) {
        super(amount + " is not a valid amount. only positive and non-zero values are allowed.");
        this.setAmount(amount);
    }

    public int getAmount() {
        return amount;
    }

    public InvalidAmountException setAmount(int amount) {
        this.amount = amount;
        return this;
    }

}

